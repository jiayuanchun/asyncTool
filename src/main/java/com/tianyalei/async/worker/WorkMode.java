package com.tianyalei.async.worker;

/**
 * @author wuweifeng wrote on 2019-11-19.
 */
public enum WorkMode {
    SELF,
    SHARE
}
